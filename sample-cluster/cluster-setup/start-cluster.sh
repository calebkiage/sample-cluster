#!/usr/bin/env bash

CLUSTER_CONFIG_FILE=${1}
rke=/vagrant/rke/rke_linux-amd64

echo "Starting up cluster..."
${rke} up --config /vagrant/cluster-setup/${CLUSTER_CONFIG_FILE}
