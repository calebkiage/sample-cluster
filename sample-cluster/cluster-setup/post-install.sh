#!/usr/bin/env bash

KUBECONFIG=${1}

echo "export KUBECONFIG=${KUBECONFIG}" > /etc/profile.d/kube-environment.sh
source /etc/profile.d/kube-environment.sh
