#!/usr/bin/env bash

CLUSTER_DOMAIN=${1:-'asgard.io'}
LB_SUBDOMAIN=${2:-'lb'}
LB_IP=${3:-'127.0.0.1'}
DNS_SERVER_IP=${4:-'0.0.0.0'}
INGRESS_SUBDOMAIN=${5:-'*.apps'}
WORKER_IPS=${6}

echo "CLUSTER_DOMAIN=$CLUSTER_DOMAIN, LB_SUBDOMAIN=$LB_SUBDOMAIN, LB_IP=$LB_IP, WORKER_IPS=$WORKER_IPS"

INGRESS_ENTRY=''

# https://stackoverflow.com/a/918931/1848555
if [[ ! -z ${WORKER_IPS} && ! -z ${INGRESS_SUBDOMAIN} ]]; then
    INGRESS_ENTRY=${INGRESS_SUBDOMAIN}$'\t\t3600'
    IFS=';' read -ra ADDR <<< ${WORKER_IPS}
    LAST_INDEX=${#ADDR[@]}-1
    for i in ${!ADDR[@]}; do
        IP=${ADDR[i]}
        PREFIX=$'\tIN\tA\t'
        INGRESS_ENTRY=${INGRESS_ENTRY}${PREFIX}${IP}
        if [[ i -lt ${LAST_INDEX} ]]; then
            SUFFIX=$'\\\n\t\t'
            INGRESS_ENTRY=${INGRESS_ENTRY}${SUFFIX}
        fi
    done
fi
echo "<<dns_ip>>" | sed -e "s/<<dns_ip>>/$INGRESS_ENTRY/g"

yum install -y bind
firewall-cmd --zone=public --add-service=dns --permanent
firewall-cmd --reload

rm -fv /etc/named.conf
rm -fv /var/named/asgard.io.zone

cp /vagrant/dns-config/etc/named.conf /etc/named.conf
cp /vagrant/dns-config/var/named/asgard.io.zone /var/named/asgard.io.zone
sed -i -e "s/<<dns_ip>>/$DNS_SERVER_IP/g" /etc/named.conf
sed -i -e "s/<<domain>>/$CLUSTER_DOMAIN/g" \
    -e "s/<<lb_subdomain>>/$LB_SUBDOMAIN/g" \
    -e "s/<<lb_ip>>/$LB_IP/g" \
    -e "s/<<ingress_wildcard_entry>>/$INGRESS_ENTRY/g" /var/named/asgard.io.zone

cat /var/named/asgard.io.zone
systemctl enable named
systemctl restart named
