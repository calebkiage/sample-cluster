#!/usr/bin/env bash

if [[ -e /usr/bin/firewall-cmd ]]; then
    firewall-cmd --zone=public --add-service=http --permanent
    firewall-cmd --zone=public --add-service=https --permanent
    firewall-cmd --reload
fi

