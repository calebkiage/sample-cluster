#!/usr/bin/env bash

firewall_service=firewalld

if ! (( $(systemctl status $firewall_service | grep -ic "Active.*\?active.*\?running") > 0 ))
then
    echo "$firewall_service service not running. Enabling & starting $firewall_service..."
    systemctl enable $firewall_service
    systemctl start $firewall_service
fi

swapoff -a

if [[ -e /usr/bin/firewall-cmd ]]; then
    firewall-cmd --zone=public --add-service=http --permanent # Port 80
    firewall-cmd --zone=public --add-service=https --permanent # Port 443
    firewall-cmd --reload
fi
