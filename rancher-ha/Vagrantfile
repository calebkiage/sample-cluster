# -*- mode: ruby -*-
# vi: set ft=ruby :
require 'yaml'

x = YAML.load_file('../config.yml')
# puts "Config: #{x.inspect}\n\n"

$rancher_ip=x['rancher']['entry-ip']
$installer_url=x['rancher']['installer-url']
$installer_checksum_url=x['rancher']['installer-checksum-url']

$helm_script=<<-SCRIPT
export HELM_INSTALL_DIR=/usr/bin
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get | bash
SCRIPT

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|

  config.vm.box = "centos/7"

  config.vm.synced_folder "../.ssh", "/vagrant/.ssh", type: "rsync"
  config.vm.synced_folder "../common", "/vagrant/common_root", type: "rsync"
  config.vm.synced_folder "../nginx-config", "/vagrant/nginx-config", type: "rsync"
  config.vm.synced_folder "../rke", "/vagrant/rke", type: "rsync"

  config.vm.provision "shell", path: "../common/ssh-keys-config.sh", privileged: false

  # Prepares each node for k8s installation.
  config.vm.provision "prepare-node", type: "shell", path: "../common/prepare-node.sh"

  # Installs docker
  config.vm.provision "install-docker", type: "docker"

  node_config=x['rancher']['ha']
  node_count=node_config['instances']
  node_ip_prefix=node_config['ip-prefix']
  node_cpus=node_config['cpus']
  node_memory=node_config['memory']
  (1..node_count).each do |i|
    config.vm.define "node-#{i}" do |node|
      node.vm.hostname = "node-#{i}"
      node.vm.network "private_network", ip: "#{node_ip_prefix}#{i}"

      node.vm.provider "virtualbox" do |vb|
        vb.memory = node_memory
        vb.cpus = node_cpus
        vb.linked_clone = true
        vb.name = "rancher-ha_#{node.vm.hostname}"
      end

      node.vm.provision "shell", path: "./nodes-config/setup.sh"
    end
  end

  config.vm.define "lb" do |lb|
    # Triggers
    lb.trigger.before :up do |t|
      re=/([^\/]+)$/m
      checksum_file_name=''

      $installer_checksum_url.scan(re) do |match|
        checksum_file_name = match
      end

      script_path = '../common/fetch-kubernetes-installer.sh'
      args = "'#{$installer_url}' '#{$installer_checksum_url}' '#{checksum_file_name[0]}'"
      t.run={ path: script_path, args: args }
    end

    lb.vm.hostname = "lb"
    lb.vm.network "private_network", ip: $rancher_ip

    lb.vm.provider "virtualbox" do |vb|
      vb.memory = 4096
      vb.cpus = 2
      vb.linked_clone = true
      vb.name = "rancher-ha_#{lb.vm.hostname}"
    end

    lb.vm.provision "nginx-setup", type: "shell", path: "../nginx-config/setup.sh"

    lb.vm.provision "initialize-rancher-cluster", type: "shell", path: "./cluster-setup/start-rancher-cluster.sh",
          run: "always",
          privileged: false

    lb.vm.provision "install-kubectl", type: "shell", path: "../common/install-kubectl.sh"

    lb.vm.provision "install-helm", type: "shell", run: "always", inline: $helm_script

    $rancher_hostname = "#{x['cluster']['lb-subdomain']}.#{x['cluster']['domain']}"
    lb.vm.provision "install-rancher", type: "shell", path: "./cluster-setup/rancher-server-install.sh",
            args: [$rancher_hostname, "/vagrant/cluster-setup/kube_config_rancher-cluster.yml"]

    lb.vm.provision "docker" do |d|
      d.run "nginx", image: "nginx:1.15-alpine",
          args: "-p 80:80 -p 443:443 -v /vagrant/nginx-config/nginx.conf:/etc/nginx/nginx.conf:ro"
    end
  end
end

Vagrant.require_version ">= 2.2.0"
