#!/usr/bin/env bash

# Download rke if it doesn't exist
rke=/vagrant/rke/rke_linux-amd64
if [[ ! -e ${rke} ]]; then
    curl -o ${rke}  -L https://github.com/rancher/rke/releases/download/v0.1.17/rke_linux-amd64
fi

chmod +x ${rke}

echo "Starting up Rancher cluster..."
${rke} up --config /vagrant/cluster-setup/rancher-cluster.yml
