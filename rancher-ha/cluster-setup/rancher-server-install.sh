#!/usr/bin/env bash

LB_HOSTNAME=${1:-localhost}
KUBECONFIG=${2}

echo "export KUBECONFIG=${KUBECONFIG}" > /etc/profile.d/kube-environment.sh
source /etc/profile.d/kube-environment.sh

kubectl -n kube-system create serviceaccount tiller

kubectl create clusterrolebinding tiller \
  --clusterrole=cluster-admin \
  --serviceaccount=kube-system:tiller

helm init --service-account tiller

kubectl -n kube-system rollout status deploy/tiller-deploy

helm repo add rancher-latest https://releases.rancher.com/server-charts/latest

helm install stable/cert-manager \
  --name cert-manager \
  --namespace kube-system \
  --version v0.5.2

kubectl -n kube-system rollout status deploy/cert-manager

if [[ ! -z ${LB_HOSTNAME} ]]; then
    # echo "No hostname set. Using default hostname '$LB_HOSTNAME'"
    echo "Using hostname '$LB_HOSTNAME'"
    helm install rancher-latest/rancher \
      --name rancher \
      --namespace cattle-system \
      --set hostname=${LB_HOSTNAME}

    kubectl -n cattle-system rollout status deploy/rancher
fi
