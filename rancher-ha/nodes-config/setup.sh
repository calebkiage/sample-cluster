#!/usr/bin/env bash

if [[ -e /usr/bin/firewall-cmd ]]; then
    firewall-cmd --zone=public --add-service=http --permanent # Port 80
    firewall-cmd --zone=public --add-service=https --permanent # Port 443
    firewall-cmd --zone=public --add-port=2376/tcp --permanent
    firewall-cmd --zone=public --add-port=2379-2380/tcp --permanent
    firewall-cmd --zone=public --add-port=6443/tcp --permanent
    firewall-cmd --zone=public --add-port=8472/udp --permanent
    firewall-cmd --zone=public --add-port=9099/tcp --permanent
    firewall-cmd --zone=public --add-port=10250/tcp --permanent
    firewall-cmd --zone=public --add-port=10254/tcp --permanent
    firewall-cmd --zone=public --add-port=30000-32767/tcp --permanent
    firewall-cmd --zone=public --add-port=30000-32767/udp --permanent
    firewall-cmd --reload
fi
