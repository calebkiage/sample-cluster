# Getting started
1. Create ssh keys in `.ssh/vagrant` to allow passwordless ssh (needed to install kubernetes).
2. Update the settings in the `config.yml` file.
3. Switch to the appropriate rancher directory by running either:
    ```bash
    # For a high availability install
    $ cd rancher-ha
    ```
    or
    ```bash
    # For single node install
    $ cd rancher-single
    ```
4. Start rancher
    ```bash
    $ vagrant up
    ```
5. If you chose to run the high availability rancher configuration,
ensure the hostname defined in the `config.yml` file is resolvable to
the nginx load balancer IP address (`192.168.50.5`). If not, add an entry
in the hosts file of the host computer
6. Navigate to rancher url (`https://192.168.50.5` or `https://<hostname>`)
and configure the rancher server.
7. You can start an additional cluster (if you dare) by running the following commands
    ```bash
    $ cd ../sample-cluster
    $ vagrant up
    ```
8. Optionally import the new cluster into Rancher.
    > To use kubectl command on the sample cluster, ssh into the `kubectl`
    node. To do that, run the command: `vagrant ssh kubectl`