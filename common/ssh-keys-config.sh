#!/usr/bin/env bash

# Setup passwordless SSH
# Run this as the vagrant user (privileged: false)
if [[ ! -e /home/vagrant/.ssh/id_rsa ]]; then
    if [[ -e /vagrant/.ssh/vagrant ]]; then
        cp /vagrant/.ssh/vagrant /home/vagrant/.ssh/id_rsa
        cp /vagrant/.ssh/vagrant.pub /home/vagrant/.ssh/id_rsa.pub
        cat /home/vagrant/.ssh/id_rsa.pub >> /home/vagrant/.ssh/authorized_keys
    fi
fi
