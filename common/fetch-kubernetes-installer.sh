#!/usr/bin/env bash

INSTALLER_URL=${1:-'https://github.com/rancher/rke/releases/download/v0.2.2/rke_linux-amd64'}
INSTALLER_CHECKSUM_URL=${2:-'https://github.com/rancher/rke/releases/download/v0.2.2/sha256sum.txt'}
CHECKSUM_REMOTE=${3:-'sha256sum.txt'}

# Downloading rancher installer
# Download rke if it doesn't exist
CHECKSUM_LOCAL=../rke/checksum.txt
CHECKSUM_REMOTE=../rke/${CHECKSUM_REMOTE}
INSTALLER_PATH=../rke/rke_linux-amd64

echo "Downloading checksum file from ${INSTALLER_CHECKSUM_URL}"
curl -sS -o ${CHECKSUM_REMOTE} -L ${INSTALLER_CHECKSUM_URL}
sed 's/build\/bin/..\/rke/' ${CHECKSUM_REMOTE} > ${CHECKSUM_LOCAL}
sha256sum --check --ignore-missing --status ${CHECKSUM_LOCAL}

if [[ $? -ne 0 ]]; then
    echo "Downloading installer file from ${INSTALLER_URL}"
    curl -sS -o ${INSTALLER_PATH} -L ${INSTALLER_URL}
    chmod +x ${INSTALLER_PATH}
else
    echo "Kubernetes installer file found."
    rm ${CHECKSUM_REMOTE}
fi
