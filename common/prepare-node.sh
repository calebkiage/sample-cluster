#!/usr/bin/env bash

echo "Preparing environment..."

firewall_service=firewalld

if ! (( $(systemctl status $firewall_service | grep -ic "Active.*\?active.*\?running") > 0 ))
then
    echo "$firewall_service service not running. Enabling & starting $firewall_service..."
    systemctl enable $firewall_service
    systemctl start $firewall_service
fi

swapoff -a

# Required modules for Rancher.
modprobe br_netfilter
modprobe ip6_udp_tunnel
modprobe ip_set
modprobe ip_set_hash_ip
modprobe ip_set_hash_net
modprobe iptable_filter
modprobe iptable_nat
modprobe iptable_mangle
modprobe iptable_raw
modprobe nf_conntrack_netlink
modprobe nf_conntrack
modprobe nf_conntrack_ipv4
modprobe nf_defrag_ipv4
modprobe nf_nat
modprobe nf_nat_ipv4
modprobe nf_nat_masquerade_ipv4
modprobe nfnetlink
modprobe udp_tunnel
modprobe veth
modprobe vxlan
modprobe x_tables
modprobe xt_addrtype
modprobe xt_conntrack
modprobe xt_comment
modprobe xt_mark
modprobe xt_multiport
modprobe xt_nat
modprobe xt_recent
modprobe xt_set
modprobe xt_statistic
modprobe xt_tcpudp

if [[ ! -e /etc/sysctl.d ]]; then
    mkdir -p /etc/sysctl.d
fi

rm -f /etc/sysctl.d/sysctl-custom.conf
cp /vagrant/common_root/sysctl-custom.conf /etc/sysctl.d/sysctl-custom.conf
sysctl -p
