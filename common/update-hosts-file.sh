#!/usr/bin/env bash

RANCHER_HOSTNAME=${1:-localhost}
RANCHER_IP=${2:-127.0.0.1}

if [[ ! -z ${RANCHER_HOSTNAME} ]]; then
    echo "$RANCHER_IP  ${RANCHER_HOSTNAME}" >> /etc/hosts
fi
