#!/usr/bin/env bash

/bin/sh /vagrant/common_root/firewall-config.etcd.sh

if [[ -e /usr/bin/firewall-cmd ]]; then
    firewall-cmd --reload
fi
